
livro = {}
livro["grokking"] = 2018
livro['rapido'] = 2007
livro['devagar'] = 2007
livro['inteligencia'] = 2003

print(livro)

votos = {}

def confere_voto(nome):
    if votos.get(nome):
        print 'Para para para'
    else:
        votos[nome] = True
        print 'vote vote vote'

#  exemplo de cache com hash
cache = {}

def get_page(url):
    if cache.get(url):
        return cache[url]  #  retorna cached data
    else:
        data = get_data_from_server(url)
        cache[url] = data  #  guarda esse dado no cache primeiro
        return data
