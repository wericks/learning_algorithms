import timeit
import random


def quicksort(array):
    inicio = timeit.default_timer()
    if len(array) < 2:
        fim = timeit.default_timer()
        print('tempo: ', fim - inicio)
        return array  # Base Case: array com 0 ou 1 elemento já ordenado
    else:
        meio = int(len(array) / 2)  # identifica o index no ponto mediano
        pivot = array[meio]
        # pivot = array[0]  # Recursive Case

        less = [i for i in array[0:] if i < pivot]  # sub-array of elements less than pivot
        greater = [i for i in array[0:] if i > pivot]  # sub-array of elements greater than pivot
        return quicksort(less) + [pivot] + quicksort(greater)


teste = []
for j in range(22):
    teste.append(random.randint(0, 88))

print(quicksort(teste))
# print(len(teste))
# print(quicksort([13, 3, 11, 19, 125, 25, 24, 30, 8, 97, 118]))
print('\n')


def quicksort1(arr):
    inicio = timeit.default_timer()
    if len(arr) < 2:
        fim = timeit.default_timer()
        print('tempo: ', fim - inicio)
        return arr
    else:
        pivo = arr[0]
        menor = [i for i in arr[1:] if i < pivo]
        maior = [i for i in arr[1:] if i > pivo]
        return quicksort1(menor) + [pivo] + quicksort1(maior)


#  dois = []
#  for j in range(22):
#    dois.append(random.randint(0, 77))

print(quicksort1(teste))

