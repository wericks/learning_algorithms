def mergeSort(arr):
    if len(arr) < 2:
        return arr
        
    meio = len(arr) / 2
    esquerda = mergeSort([meio:])
    direita = mergeSort([:meio])
    
    return merge(esquerda, direita)
   
   
def merge(esquerda, direita):
    resultado = []
    
    while len(esquerda) > 0 and len(direita) > 0:
        if esquerda[0] <= direita[0]:
            resultado.append(esquerda[0])
            esquerda = esquerda[1:]
        else:
            resultado.append(direita[0])
            direita = direita[1:]
            
    if len(esquerda) > 0:
        resultado.append(esquerda)
    if len(direita) > 0:
        resultado.append(direita)
    
    return resultado

