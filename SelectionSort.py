# class SelectionSort:
#     def __init__(self, lista, tamanho):
#
#         for i in range(tamanho):
#             index_menor = i
#
#             for j in range(i + 1, tamanho):
#                 if lista[j] < lista[index_menor]:
#                     index_menor = j
#
#             (lista[i], lista[index_menor]) = (lista[index_menor], lista[i])
#         return lista

# arr = [55, 554, 9, 24, -1, -43, 2, 3, 2, 1, 5, 55, 65, 12, 9, 8, 43, 30, 28]
# tamanho = len(arr)
# selection_sort(arr, tamanho)

# print(arr)

def selection_sort(arr):  # ordena a array
    nova_arr = []
    for i in range(len(arr)):
        menor = encontrar_menor(arr)  # encontra o menor elemento na array, e adiciona-o a nova array
        nova_arr.append(arr.pop(menor))
    return nova_arr


def encontrar_menor(arr):
    menor = arr[0]  # guarda o menor valor
    menor_index = 0  # guarda o menor index
    for i in range(1, len(arr)):
        if arr[i] < menor:
            menor = arr[i]
            menor_index = i
    return menor_index


print(selection_sort([5, 7, 8, 87, 13, 28, 1, 0.9, 0.12, 1.1, 1.0, 9, 10, 24, 8, 30]), "\n")

test_arr = [55, 554, 9, 24, -1, -43, 2, 3, 2, 1, 5, 55, 65, 12, 9, 8, 43, 30, 28]
print(selection_sort(test_arr))
