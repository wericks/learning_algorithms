def binary_search(item, lista_ordenada):
    baixo = 0
    alto = len(lista_ordenada) - 1

    while baixo <= alto:
        meio = (baixo + alto)
        palpite = lista_ordenada[meio]
        if palpite == item:
            return meio
        if palpite > item:
            alto = meio - 1
        else:
            baixo = meio + 1
    return None



minha_list = [1, 2, 4, 5, 6, 8, 9, 11, 13]


print(binary_search(55, minha_list))
print(binary_search(8, minha_list))
print(binary_search(1, minha_list))
